package com.antonov247;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TextMarker
{
    private final String originalString;
    private Map<Integer, List<String>> modifiers;
    private List<TextModifier> modifierList = new ArrayList<>();


    public TextMarker(String s)
    {
        this.originalString = s;
    }

    public void addModifier(TextModifier modifier)
    {
        modifierList.add(modifier);
        if (modifiers == null)
        {
            modifiers = new TreeMap<>();
        }
        modifiers.computeIfAbsent(modifier.start, k -> new ArrayList<>());
        modifiers.get(modifier.start).add(modifier.value);
        modifiers.computeIfAbsent(modifier.end, k -> new ArrayList<>());
        List<TextModifier> list = getModifiers(modifier.start);
        int index = modifier.end;
        if (!list.isEmpty())
        {
            index = getFirstClosedTagIndex(list);
            modifier.end = index;
        }
        modifiers.get(index).add(modifier.valEnd);
    }

    public void clearModifiers(){
        modifierList.clear();
        if (modifiers != null) {
            modifiers.clear();
        }
    }

    private int getFirstClosedTagIndex(List<TextModifier> list)
    {
        int min = list.get(0).end;
        for (TextModifier m : list)
        {
            if (m.end < min) min = m.end;
        }
        return min;
    }

    public List<TextModifier> getModifiers(int i)
    {
        List<TextModifier> result = new ArrayList<>();
        for (TextModifier modifier : modifierList)
        {
            if (modifier.start <= i && modifier.end >= i)
            {
                result.add(modifier);
            }
        }
        return result;
    }

    public void deleteModifier(TextModifier modifier)
    {

    }

    public String getModifiedString()
    {
        StringBuilder stringBuilder = new StringBuilder(originalString);
        int offset = 0;
        for (Map.Entry<Integer, List<String>> entry : modifiers.entrySet())
        {
            String s = generatePluralModifier(entry.getValue());
            stringBuilder.insert(entry.getKey() + offset, s);
            offset += s.length();
        }
        return stringBuilder.toString();
    }

    private String generatePluralModifier(List<String> list)
    {
        StringBuilder result = new StringBuilder();
        StringBuilder closeTags = new StringBuilder();
        StringBuilder openTags = new StringBuilder();

        for (String s : list)
        {
            if (s.startsWith("/"))
            {
                closeTags.append("{/}");
            }
            else if (!s.isEmpty())
            {
                openTags.append(s).append(";");
            }
        }
        result.append(closeTags);
        if (openTags.length() > 0)
        {
            openTags.deleteCharAt(openTags.length() - 1);
            result.append("{").append(openTags).append("}");
        }
        return result.toString();
    }

    public static class TextModifier
    {
        int start;
        int end;
        String value;
        String valEnd;

        public TextModifier(int start, int end, String value)
        {
            this.start = start;
            this.end = end;
            this.value = value;
            if (start == end)
            {
                this.valEnd = "";
            }
            else
            {
                this.valEnd = "/";
            }
        }
    }

    public String getOriginalString()
    {
        return originalString;
    }


}
