package com.antonov247;

import com.antonov247.controller.MainFrameController;

public class ReachTextApplication {

    public static void main(String[] args) {
        MainFrameController controller = new MainFrameController();
        controller.showMainFrameWindow();
    }

}
