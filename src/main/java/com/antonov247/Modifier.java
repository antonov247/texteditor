package com.antonov247;

public enum Modifier
{
    color,
    style,
    outline,
    outline_color,
    name,
    size;

    public String of(Object value){
        return name()+":"+value;
    }
}
