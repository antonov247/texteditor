package com.antonov247.controller;

import com.antonov247.Modifier;
import com.antonov247.TextMarker;
import com.antonov247.view.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrameController {
    private MainFrame frame;
    private JButton clearButton;
    private JButton runButton;
    private JComboBox<String> fontJComboBox;
    private JComboBox<MainFrame.FontStyle> fontStyleJComboBox;
    private JTextArea sourceTextArea;
    private JTextArea resultTextArea;
    private TextMarker textMarker;
    private JColorChooser jColorChooser;
    private JButton colorChooser;
    private String defaultString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta lacus at lorem luctus, eget placerat eros cursus. Donec a molestie dui, at imperdiet lectus. Praesent diam massa, elementum sit.";


    public MainFrameController() {
        initComponents();
        initListeners();
    }

    private void initComponents() {
        frame = new MainFrame();
        clearButton = frame.getClearButton();
        runButton = frame.getRunButton();
        fontJComboBox = frame.getFontJComboBox();
        fontStyleJComboBox = frame.getFontStyleJComboBox();
        sourceTextArea = frame.getSourceTextArea();
        resultTextArea = frame.getResultTextArea();
        textMarker = new TextMarker(defaultString);
        colorChooser = frame.getColorChooser();
//        jColorChooser = new JColorChooser();
//        jColorChooser.setBorder(BorderFactory.createTitledBorder(
//                "Choose Text Color"));
//
//       frame.getMainPannel().add(jColorChooser);

    }

    private void initListeners() {
        clearButton.addActionListener(e -> {
            textMarker.clearModifiers();
            resultTextArea.setText("");
        });

        runButton.addActionListener(e -> {
            int start = sourceTextArea.getSelectionStart();
            int end = sourceTextArea.getSelectionEnd();
            String font = (String) fontJComboBox.getModel().getSelectedItem();
            MainFrame.FontStyle style = (MainFrame.FontStyle) fontStyleJComboBox.getModel().getSelectedItem();
            textMarker.addModifier(new TextMarker.TextModifier(start, end, Modifier.name.of(font)));
            textMarker.addModifier(new TextMarker.TextModifier(start, end, Modifier.style.of(style.toString())));
            resultTextArea.setText(textMarker.getModifiedString());
        });

        colorChooser.addActionListener(e -> {
//            Color color = jColorChooser.getColor();
            Color color = JColorChooser.showDialog(frame, "Color", null);
            if (color != null) {
                String hex = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
                System.out.println("color = " + hex);
            }
        });
    }

    public void showMainFrameWindow() {
        frame.setVisible(true);
    }
}