package com.antonov247.view;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class MainFrame extends JFrame {

    private static final String defaultString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
            " Sed porta lacus at lorem luctus, eget placerat eros cursus. Donec a molestie dui, at imperdiet lectus." +
            " Praesent diam massa, elementum sit.";
    private JButton clearButton;
    private JPanel mainPannel;
    private JButton runButton;
    private JComboBox<String> fontJComboBox;
    private JComboBox<FontStyle> fontStyleJComboBox;
    private JTextArea sourceTextArea;
    private JTextArea resultTextArea;
    private JButton colorChooser;


    public MainFrame() {
        setTitle("Editor");
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension windowSize = toolkit.getScreenSize();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds((windowSize.width - 800) / 2, (windowSize.height - 600) / 2, 800, 600);
        createPanel();
    }

    public JButton getColorChooser() {
        return colorChooser;
    }

    private void createPanel() {

        setContentPane(mainPannel);
        createFontComboBox();
        createFontStyleComboBox();

        IconFontSwing.register(FontAwesome.getIconFont());
        Icon icon = IconFontSwing.buildIcon(FontAwesome.ERASER, 20, new Color(255, 0, 0));
        clearButton.setIcon(icon);
        Icon icon1 = IconFontSwing.buildIcon(FontAwesome.PLAY, 20, new Color(0, 150, 0));
        runButton.setIcon(icon1);
        sourceTextArea.setLineWrap(true);
        resultTextArea.setLineWrap(true);

    }

    private void createFontStyleComboBox() {
        Arrays.stream(FontStyle.values()).forEach(fontStyleJComboBox::addItem);
    }

    private void createFontComboBox() {
        Arrays.stream(getFonts()).forEach(a -> fontJComboBox.addItem(a.getFamily()));
    }

    private Font[] getFonts() {
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
    }

    private JButton createButton(String s) {
        JButton button = new JButton(s);
        button.setSize(50, 20);
        return button;
    }

    public enum FontStyle {
        Regular, Bold, Italic;
    }

    public String getDefaultString() {
        return defaultString;
    }

    public JButton getClearButton() {
        return clearButton;
    }

    public JPanel getMainPannel() {
        return mainPannel;
    }

    public JButton getRunButton() {
        return runButton;
    }

    public JComboBox<String> getFontJComboBox() {
        return fontJComboBox;
    }

    public JComboBox<FontStyle> getFontStyleJComboBox() {
        return fontStyleJComboBox;
    }

    public JTextArea getSourceTextArea() {
        return sourceTextArea;
    }

    public JTextArea getResultTextArea() {
        return resultTextArea;
    }
}
